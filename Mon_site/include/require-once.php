<?php
// début index.php
echo ('<div>Notre paragraphe inclus :</div>');
// Inclusion du paragraphe présent dans le fichier.inc.php
require_once('./fichier.inc.php'); // si fichier inexistant : une erreur non bloquante
// include_once('./fichier.inc.php'); // si fichier inexistant : erreur bloquante!!!
// idem que :
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ea hic debitis quo nemo, quasi quis iure tempore Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quibusdam temporibus ipsa et quod distinctio. Facere adipisci voluptas nostrum, ab accusantium nemo repudiandae impedit cum similique, iure et, rem dolor?</p>
// retour dans notre index.php
echo ('<div>Retour dans notre require-once.php</div>');
